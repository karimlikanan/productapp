import './navbar.html'

import {
    Template
} from 'meteor/templating';
import {
    ReactiveVar
} from 'meteor/reactive-var';

import {Categories} from '../../api/categories/collections';


Template.navbar.helpers({
    loggedIn() {
         return Meteor.userId();
    }
});