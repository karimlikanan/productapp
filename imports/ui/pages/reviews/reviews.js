import './reviews.html'
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import {
    FlowRouter
  } from 'meteor/ostrio:flow-router-extra';

  import {Products} from '../../../api/products/collections';

  import {Images} from '../../../api/images/collection'

  import {Reviews} from '../../../api/reviews/collections';

  Template.reviews.onCreated(function () {
    // counter starts at 0


    this.productId = new ReactiveVar();
    this.productRat = new ReactiveVar(0)
    this.avrRat = new ReactiveVar(0)
    this.rat = new ReactiveVar()

    this.autorun(() => {
        this.subscribe('getImages')
      this.subscribe('getLastReviews', this.productId.get())
      this.subscribe('getProducts')

      FlowRouter.watchPathChange()
      this.productId.set(FlowRouter.getParam('_id'));

      
      Meteor.call('getCount',this.productId.get(), (err,res)=>{
        if(err) alert(err)
        if(res) {
         this.avrRat.set(res)}
      })
  
  });


  });

  Template.reviews.helpers({
    getProduct(){
     return  Products.findOne({_id: Template.instance().productId.get()});
    },
    allReviews() {
      return Reviews.find({productId: Template.instance().productId.get()}, {
        sort: {dateTime:-1},limit: 3
    });
  },
  getImage(){
    return Images.findOne({_id:this.imageId})?.link();
},
  dateTime(){
      return moment(this.dateTime).format('MM-DD-YYYY')
  },
  goldStar(){
    let rating = this.rating;
    let arr = new Array(rating);
    for (let i = 0; i < rating; i++) {
        arr[i]= i;
      
    }
    return arr;

  },
  whiteStar(){
    let rating = 5-this.rating;
    let arr = new Array(rating);
    for (let i = 0; i < rating; i++) {
        arr[i]= i;
      
    }
    return arr;

  },

  avrGoldStar(){
    let product = Products.findOne({
      _id : Template.instance().productId.get()
    })
    if(!product){
      return ;
    }
    Template.instance().productRat.set(product?.avgRating)


    let rating = Template.instance().productRat.get();
    return StarFiller(rating)
  },
  avrWhiteStar(){
    let product = Products.findOne({
      _id : Template.instance().productId.get()
    })
    
    if(!product){
      return ;
    }

    let rating = 5 - Template.instance().productRat.get();
   return StarFiller(rating)

  },
  getRevCount(){
    return Template.instance().avrRat.get()
    
   
  },
  getAvgRating(){
    return Template.instance().rat.get()
  }


    
  });
  

  function StarFiller(rating = 0) {
    let arr = new Array(rating);

    for (let index = 0; index < rating; index++) {
        arr[index] = 1;
    }

    return arr;
}