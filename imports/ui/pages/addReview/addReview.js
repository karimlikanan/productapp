import './addReview.html'

import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import {
    FlowRouter
  } from 'meteor/ostrio:flow-router-extra';

  import {Products} from '../../../api/products/collections';

  import {Reviews} from '../../../api/reviews/collections';

  
  
Template.addReview.onCreated(function () {

  this.productId = new ReactiveVar();
    // counter starts at 0
    this.autorun(() => {

      this.subscribe('getReviews')
      this.subscribe('getProducts')

  });
  });
  
  Template.addReview.helpers({
    getProduct(){
      Template.instance().productId.set(FlowRouter.getParam('_id'));
     return  Products.findOne({_id: Template.instance().productId.get()});
    },
    allReviews() {
      return Reviews.find({});
  },
//   format(){
//      let review =Reviews.findOne({_id:'3SmQZ25eBNFcxNmzE'})?.dateTime;
//       return moment(review).format('MM-DD-YYYY')
//   }

    
  });

  Template.addReview.events({
    'submit #addReviewForm'(e, instance) {
      e.preventDefault();
        let target = e.target;
        let productId = Template.instance().productId.get();
        let rating = Number.parseInt(target.rating.value);
        let reviewText = target.review.value;

        let review = {
            productId: productId,
            rating: rating,
            reviewText:reviewText,
            dateTime: new Date()
        }

        Meteor.call('addReview', review, (err,res) =>{
            if(err) alert(err)
            if(res) FlowRouter.go('/reviews/'+ productId)
        })


}
});
