import {
  Template
} from 'meteor/templating';
import {
  ReactiveVar
} from 'meteor/reactive-var';
import {
  FlowRouter
} from 'meteor/ostrio:flow-router-extra';

import './home.html'

import {
  Images
} from '../../../api/images/collection'

import {
  Products
} from '../../../api/products/collections';

import {
  ProductsReviews
} from '../../../api/productsReviews/collections'


Template.home.onCreated(function () {
      // counter starts at 0


      // this.avrRat = new ReactiveVar(0)
      this.productId = new ReactiveVar();


      this.autorun(() => {
        this.subscribe('getImages')
        this.subscribe('getProducts')



        if (Products.find().count()) {
          ProductsReviews.remove()

          Products.find().map((el) => {
              Meteor.call('addProductsReviews', el._id, (err, res) => {
                if (err) alert(err)
                if (res != undefined) {
                  ProductsReviews.insert({
                    productId: el._id,
                    revCount: res})
                  }
                })
          })
            }


          
            // Meteor.call('addProductsReviews',this.productId.get(), (err,res)=>{
            //   if(err) alert(err)
            //   if(res) {
            //    console.log(res)}
            // })

            // Meteor.call('getCount',this.productId.get(), (err,res)=>{
            //   if(err) alert(err)
            //   if(res) {
            //    this.avrRat.set(res)}
            // })
          });
      });

      Template.home.helpers({
        allProducts() {
          return Products.find({
            isFeatured: false
          });

        },
        featuredPro() {
          return Products.find({
            isFeatured: true
          });
        },
        getImage() {
          return Images.findOne({
            _id: this.imageId
          }) ?.link();
        },
        getRouterName() {
          return FlowRouter.getRouteName();
        },
        loggedIn() {
          return Meteor.userId();
        },
        getRevCount() {
          // Template.instance().productId.set(this._id);  

          let count = ProductsReviews.findOne({
            productId: this._id
          })

          return count?.revCount;


        },
        avrGoldStar() {
          let rating = this.avgRating;
          return StarFiller(rating)
        },
        avrWhiteStar() {
          let rating = this.avgRating;
          return StarFiller((5 - rating))
        },


      })


      function StarFiller(rating = 0) {
        let arr = new Array(rating);

        for (let index = 0; index < rating; index++) {
          arr[index] = 1;
        }

        return arr;
      }