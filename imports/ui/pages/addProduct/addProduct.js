import './addProduct.html'

import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import {Images} from '../../../api/images/collection'

import {Categories} from '../../../api/categories/collections';

import {Products} from '../../../api/products/collections';

import {
  FlowRouter
} from 'meteor/ostrio:flow-router-extra';



Template.addProduct.onCreated(function () {
  // counter starts at 0
  this.loading = new ReactiveVar(false);

  this.autorun(() => {
    this.subscribe('getImages')
    this.subscribe('getCategories')
    this.subscribe('getProducts')
});
});

Template.addProduct.helpers({
  loading(){
    return Template.instance().loading.get()
  },
  allCategories() {
    return Categories.find({});
}
  // imageFile() {
  //   return Images.findOne();
  // },
});

Template.addProduct.events({
  'submit #addProductForm'(e, instance) {
    e.preventDefault();

    let target = e.target;
    let name = target.name.value;
    let categoryId = target.categoryId.value;
    let description = target.description.value;
    let isFeatured = target.featured.value ?true : false;


    // increment the counter when button is clicked
    let file=document.getElementById('myFile').files[0];

    const upload = Images.insert({
      file,
      chunkSize: 'dynamic'
    }, false);

    upload.on('start', function () {
      instance.loading.set(this);

    });

    upload.on('end', function (error, fileObj) {
      if (error) {
        alert(`Error during upload: ${error}`);
      } else {
        console.log(`File "${fileObj.name}" successfully uploaded`);

        let product={
          name:name,
          description:description,
          categoryId : categoryId,
          isFeatured:isFeatured,
          imageId: fileObj._id,
          avgRating : 0
        }
    Meteor.call('addProducts', product, (err,res)=>{
      if(err) alert(err)
      if(res) FlowRouter.go('home')
    })
      }
      instance.loading.set(false);
    });

    upload.start();

    
    

  }
});
