import '../../api/categories/collections'
import '../../api/categories/methods'
import '../../api/categories/publications'

import '../../api/products/collections'
import '../../api/products/methods'
import '../../api/products/publications'

import '../../api/reviews/collections'
import '../../api/reviews/methods'
import '../../api/reviews/publications'

import '../../api/images/collection'
import '../../api/images/publications'

import '../../api/productsReviews/collections'
import '../../api/productsReviews/metods'
