import {
  FlowRouter
} from 'meteor/ostrio:flow-router-extra';

import '../../ui/pages/addProduct/addProduct'
import '../../ui/pages/addReview/addReview'
import '../../ui/pages/home/home'
import '../../ui/pages/reviews/reviews'
import '../../ui/layouts/layout'
import '../../ui/pages/category/category'



FlowRouter.triggers.enter([checkLoggedIn], {only: ["addProduct"]});


function checkLoggedIn(ctx, redirect){
  if(!Meteor.userId()){
    redirect('/');
  }
}
FlowRouter.route('/', {
  name:'home',
  action() {

    BlazeLayout.render('MainLayout', {main:"home"});

  }
});

FlowRouter.route('/products', {
  name:'products',
  action() {

    BlazeLayout.render('MainLayout', {main:"home"});

  }
});

FlowRouter.route('/addProduct', {
  name:'addProduct',
  action() {

    BlazeLayout.render('MainLayout', {main:"addProduct"});

  }
});


FlowRouter.route('/addReview/:_id', {
  name:'addReview',
  action() {

    BlazeLayout.render('MainLayout', {main:"addReview"});

  }
});

FlowRouter.route('/reviews/:_id', {
  name:'reviews',
  action() {

    BlazeLayout.render('MainLayout', {main:"reviews"});

  }
});

FlowRouter.route('/category/:_id', {
  name:'category',
  action() {

    BlazeLayout.render('MainLayout', {main:"category"});

  }
});

// FlowRouter.route('/test', {
//     name:'test',
//   action() {
//     // Show 404 error page using Blaze
//     this.render('MainLayout', {main: 'Test'});

//     // Can be used with BlazeLayout,
//     // and ReactLayout for React-based apps
//   }
// });