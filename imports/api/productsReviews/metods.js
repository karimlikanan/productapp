import {
    ProductsReviews
} from "./collections";

import {
    Reviews
} from "../reviews/collections";


Meteor.methods({
    addProductsReviews: function (productId) {
        let count = Reviews.find({
            productId: productId
        }).count();
        return count;
    }
});