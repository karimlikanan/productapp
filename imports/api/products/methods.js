import {
    Products
} from "./collections";

Meteor.methods({
    addProducts(data) {
        return Products.insert(data)
    },
    removeProducts(_id) {
        return Products.remove({
            _id: _id
        })
    },
    // updateProducts(id,isPrivate){
    //     Products.update(id,{$set:{'isPrivate': !isPrivate}})
    // }

})