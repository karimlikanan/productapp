import {Images} from './collection';


Meteor.publish('getImages', function () {
    return Images.find().cursor;
});