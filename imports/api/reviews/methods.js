import {
    Reviews
} from "./collections";


import {Products} from '../products/collections';

Meteor.methods({
   
    // removeReviews(_id) {
    //     return Reviews.remove({
    //         _id: _id
    //     })
    // },
    // updateReviews(id,isPrivate){
    //     Reviews.update(id,{$set:{'isPrivate': !isPrivate}})
    // }
    getCount: function(productId){
        return Reviews.find({
            productId: productId
        }).count()
    },

    addReview: function (data) {

        let rev = Reviews.find({
            productId: data.productId
        });
        let count = rev.count();

        let sum = 0;
        rev.forEach(el => {
            sum += el.rating
        });

        sum += data.rating;

        let res = Math.round(sum / (count + 1));

        let _id = data.productId

        Products.update({
            _id
        }, {
            $set: {
                avgRating: res
            }
        })
        return Reviews.insert(data);
    },

})